package uz.pdp.appticket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appticket.payload.*;
import uz.pdp.appticket.service.AuthService;

@RestController
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {

    private final AuthService authService;

    @Override
    public ApiResult<TokenDto> signIn(LoginDto loginDto) {
        return authService.signIn(loginDto);
    }

    @Override
    public ApiResult<?> checkPhoneNumber(PhoneNumberDto phoneNumberDto) {
        return authService.checkPhoneNumber(phoneNumberDto);
    }

    @Override
    public ApiResult<RegisterDto> checkCode(CodeDto codeDto) {
        return authService.checkCode(codeDto);
    }

    @Override
    public ApiResult<TokenDto> signUp(SignUpDto signUpDto) {
        return authService.signUp(signUpDto);
    }

    @Override
    public ApiResult<TokenDto> refreshToken(TokenDto tokenDto) {
        return authService.refreshToken(tokenDto);
    }
}
