package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.TicketResDto;
import uz.pdp.appticket.payload.ticket.TicketEditReqDto;
import uz.pdp.appticket.payload.ticket.TicketReqDto;
import uz.pdp.appticket.utils.AppConstant;
import uz.pdp.appticket.utils.RestConstant;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.TICKET_CONTROLLER)
@Tag(name = "Chipta operatsiyalari", description = "Chipta")
public interface TicketController {

    @Operation(summary = "Chipta qo'shish")
    @PostMapping()
    ApiResult<TicketResDto> add(@RequestBody TicketReqDto ticketReqDto);

    @Operation(summary = "Chiptalarning listini sahifalab olish")
    @GetMapping()
    ApiResult<CustomPage<TicketResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                               @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Bitta chiptani olish")
    @GetMapping("/{id}")
    ApiResult<TicketResDto> getOne(@PathVariable UUID id);

    @Operation(summary = "Chiptani edit qilish")
    @PutMapping("/{id}")
    ApiResult<TicketResDto> edit(@PathVariable UUID id, @RequestBody @Valid TicketEditReqDto ticketEditReqDto);

    @Operation(summary = "Chiptani o'chirish")
    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
