package uz.pdp.appticket.controller;

import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appticket.entity.User;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.RefundReqDto;
import uz.pdp.appticket.payload.RefundResDto;
import uz.pdp.appticket.service.PayBackService;

@RestController
public class PayBackControllerImpl implements PayBackController {
    private final PayBackService payBackService;

    public PayBackControllerImpl(PayBackService payBackService) {
        this.payBackService = payBackService;
    }

    @Override
    public ApiResult<RefundResDto> refund(RefundReqDto refundReqDto, User user) {
        return payBackService.refund(refundReqDto, user);
    }
}
