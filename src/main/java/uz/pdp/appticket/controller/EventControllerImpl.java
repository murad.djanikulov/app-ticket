package uz.pdp.appticket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.EventReqDto;
import uz.pdp.appticket.payload.EventResDTO;
import uz.pdp.appticket.service.EventService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class EventControllerImpl implements EventController {

    private final EventService eventService;

    @Override
    public ApiResult<CustomPage<EventResDTO>> getAll(int page, int size) {
        return eventService.getAll(page, size);
    }

    @Override
    public ApiResult<EventResDTO> getOne(UUID id) {
        return eventService.getOne(id);
    }

    @PreAuthorize(value = "hasAuthority('ADD_EVENT')")
    @Override
    public ApiResult<EventResDTO> add(EventReqDto eventReqDto) {
        return eventService.add(eventReqDto);
    }

    @PreAuthorize(value = "hasAuthority('EDIT_EVENT')")
    @Override
    public ApiResult<EventResDTO> edit(EventReqDto eventReqDto, UUID id) {
        return eventService.edit(eventReqDto, id);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_EVENT')")
    @Override
    public ApiResult<?> delete(UUID id) {
        return eventService.delete(id);
    }
}
