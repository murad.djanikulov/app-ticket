package uz.pdp.appticket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.appticket.entity.Attachment;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AttachmentControllerImpl implements AttachmentController {

    private final AttachmentService attachmentService;


    @Override
    public ApiResult<?> upload(MultipartHttpServletRequest request, MultipartFile file) throws IOException {
        return attachmentService.upload(request);
    }

    @Override
    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        return attachmentService.getAll(page, size);
    }


    @Override
    public ApiResult<Attachment> getById(UUID id) {
        return attachmentService.getById(id);
    }

    @Override
    public ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException {
        return attachmentService.getFile(id, response);

    }
}
