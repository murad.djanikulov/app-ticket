package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.appticket.entity.Attachment;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.utils.AppConstant;
import uz.pdp.appticket.utils.RestConstant;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RequestMapping(RestConstant.ATTACHMENT_CONTROLLER)
@Tag(name = "Rasmlar bilan ishlovchi operatsiyalar", description = "Rasmlar")
public interface AttachmentController {


    @Operation(summary = "Rasmni tizimga yuklash")
    @PostMapping(value = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    ApiResult<?> upload(@Parameter MultipartHttpServletRequest request, @RequestPart(value = "file") MultipartFile file) throws IOException;

    @Operation(summary = "Rasmlar ma'lumotlarining listini sahifalab olish")
    @GetMapping("/info")
    ApiResult<CustomPage<Attachment>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Rasmning ma'lumotini olish")
    @GetMapping("/info/{id}")
    ApiResult<Attachment> getById(@PathVariable UUID id);

    @Operation(summary = "Rasmni yuklab olish")
    @GetMapping("/download/{id}")
    ApiResult<?> getFile(@PathVariable UUID id, HttpServletResponse response) throws IOException;

}
