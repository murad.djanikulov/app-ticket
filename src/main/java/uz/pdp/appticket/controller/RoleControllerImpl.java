package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appticket.enums.PermissionEnum;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.RoleAttachDto;
import uz.pdp.appticket.payload.RoleReqDto;
import uz.pdp.appticket.payload.RoleResDto;
import uz.pdp.appticket.service.RoleService;

import java.util.Set;
import java.util.UUID;

@RestController
public class RoleControllerImpl implements RoleController {
    private final RoleService roleService;

    public RoleControllerImpl(RoleService roleService) {
        this.roleService = roleService;
    }

    @Operation(summary = "Tadbirlarning listini sahifalab olish")
    @Override
    public ApiResult<RoleResDto> add(RoleReqDto roleReqDto) {
        return roleService.add(roleReqDto);
    }

    @Override
    public ApiResult<?> attachRole(RoleAttachDto roleAttachDto) {
        return roleService.attachRole(roleAttachDto);
    }

    @Override
    public ApiResult<?> getAll() {
        return roleService.getAll();
    }

    @Override
    public ApiResult<RoleResDto> getPermissionById(UUID id) {
        return roleService.getPermissionsByRoleId(id);
    }

    @Override
    public ApiResult<Set<PermissionEnum>> getAllPermission() {
        return roleService.getPermissions();
    }
}
