package uz.pdp.appticket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.TicketResDto;
import uz.pdp.appticket.payload.ticket.TicketEditReqDto;
import uz.pdp.appticket.payload.ticket.TicketReqDto;
import uz.pdp.appticket.service.TicketService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class TicketControllerImpl implements TicketController {
    private final TicketService ticketService;

    @PreAuthorize(value = "hasAuthority('ADD_TICKET')")
    @Override
    public ApiResult<TicketResDto> add(TicketReqDto ticketReqDto) {

        return ticketService.add(ticketReqDto);
    }

    @Override
    public ApiResult<CustomPage<TicketResDto>> getAll(int page, int size) {
        return ticketService.getAll(page, size);
    }

    @Override
    public ApiResult<TicketResDto> getOne(UUID id) {
        return ticketService.getOne(id);
    }

    @PreAuthorize(value = "hasAuthority('EDIT_TICKET')")
    @Override
    public ApiResult<TicketResDto> edit(UUID id, TicketEditReqDto ticketEditReqDto) {
        return ticketService.edit(id, ticketEditReqDto);
    }

    @PreAuthorize(value = "hasAuthority('DELETE_TICKET')")
    @Override
    public ApiResult<?> delete(UUID id) {
        return ticketService.delete(id);
    }
}
