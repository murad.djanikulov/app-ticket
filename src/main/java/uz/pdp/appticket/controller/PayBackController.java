package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.appticket.entity.User;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.RefundReqDto;
import uz.pdp.appticket.payload.RefundResDto;
import uz.pdp.appticket.security.CurrentUser;
import uz.pdp.appticket.utils.RestConstant;

import javax.validation.Valid;

@RequestMapping(RestConstant.PAY_BACK_CONTROLLER)
@Tag(name = "Pul qaytarish operatsiyalari", description = "Pul qaytarish")
public interface PayBackController {


    /**
     * /refund yo'li bu pulni qaytaradigan yo'l.Bu yo'lga cardnumber, userId, List<Ticket> kirib keladi
     *
     * @param refundReqDto
     * @param user
     * @return
     */

    @Operation(summary = "Pulni qayatarish")
    @PostMapping("/refund")
    ApiResult<RefundResDto> refund(@RequestBody @Valid RefundReqDto refundReqDto,
                                   @Parameter(hidden = true) @CurrentUser User user);

}
