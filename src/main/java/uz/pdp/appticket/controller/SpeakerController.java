package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SpeakerReqDto;
import uz.pdp.appticket.payload.SpeakerResDto;
import uz.pdp.appticket.utils.AppConstant;
import uz.pdp.appticket.utils.RestConstant;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.SPEAKER_CONTROLLER)
@Tag(name = " Speaker operatsiyalari", description = "Speaker")
public interface SpeakerController {

    @Operation(summary = "Speakerlarning listini sahifalab olish")
    @GetMapping
    ApiResult<CustomPage<SpeakerResDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Bitta Speakerni olish")
    @GetMapping("/{id}")
    ApiResult<SpeakerResDto> getOne(@PathVariable UUID id);

    @Operation(summary = "Speaker qo'shish")
    @PostMapping
    ApiResult<SpeakerResDto> add(@RequestBody @Valid SpeakerReqDto speakerDto);

    @Operation(summary = "Speakerni edit qilish")
    @PutMapping("/{id}")
    ApiResult<SpeakerResDto> edit(@RequestBody @Valid SpeakerReqDto speakerDto, @PathVariable UUID id);

    @Operation(summary = "Speakerni delete qilish")
    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);
}
