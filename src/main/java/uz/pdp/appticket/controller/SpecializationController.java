package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SpecializationDto;
import uz.pdp.appticket.payload.SpecializationReqDto;
import uz.pdp.appticket.utils.AppConstant;
import uz.pdp.appticket.utils.RestConstant;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.SPECIALIZATION_CONTROLLER)
@Tag(name = "Mutahasislik operatsiyalari", description = "Mutahasislik")
public interface SpecializationController {

    @Operation(summary = "Mutahasislikalarning listini sahifalab olish")
    @GetMapping
    ApiResult<CustomPage<SpecializationDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                    @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Bitta mutahasislik olish")
    @GetMapping("/{uuid}")
    ApiResult<SpecializationDto> getOne(@PathVariable UUID uuid);

    @Operation(summary = "Mutahasislikni qo'shish")
    @PostMapping
    ApiResult<SpecializationDto> add(@RequestBody @Valid SpecializationReqDto specializationReqDto);

    @Operation(summary = "Mutahasislikni edit qilish")
    @PutMapping("/{uuid}")
    ApiResult<SpecializationDto> edit(@RequestBody @Valid SpecializationReqDto specializationReqDto, @PathVariable UUID uuid);

    @Operation(summary = "Mutahasislikni o'chirish")
    @DeleteMapping("/{uuid}")
    ApiResult<?> delete(@RequestBody @Valid UUID uuid);
}
