package uz.pdp.appticket.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.PayBackTariffDto;
import uz.pdp.appticket.payload.PayBackTariffReqDto;
import uz.pdp.appticket.utils.AppConstant;
import uz.pdp.appticket.utils.RestConstant;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.PAY_BACK_TARIFF_CONTROLLER)
@Tag(name = "Pul qaytarish tariffi operatsiyalari", description = "Pul qaytarish tariffi")
public interface PayBackTariffController {

    @Operation(summary = "Pul qaytarish tarifflarining listini sahifalab olish")
    @GetMapping
    ApiResult<CustomPage<PayBackTariffDto>> getAll(@RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_NUMBER) int page,
                                                   @RequestParam(defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size);

    @Operation(summary = "Bitta pul qaytarish tariffini olish")
    @GetMapping("/{payBackTariffId}")
    ApiResult<PayBackTariffDto> getById(@PathVariable UUID payBackTariffId);

    @Operation(summary = "Pul qaytarish tariffini qo'shish")
    @PostMapping
    ApiResult<PayBackTariffDto> save(@RequestBody @Valid PayBackTariffReqDto payBackTariffReqDto);

    @Operation(summary = "Pul qaytarish tariffini edit qilish")
    @PutMapping("/{payBackTariffId}")
    ApiResult<PayBackTariffDto> edit(@RequestBody @Valid PayBackTariffReqDto payBackTariffReqDto, @PathVariable UUID payBackTariffId);

    @Operation(summary = "Pul qaytarish tariffini o'chirish")
    @DeleteMapping("/{payBackTariffId}")
    ApiResult<?> delete(@PathVariable UUID payBackTariffId);

}
