package uz.pdp.appticket.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.appticket.entity.Role;
import uz.pdp.appticket.entity.User;
import uz.pdp.appticket.enums.PermissionEnum;
import uz.pdp.appticket.exception.RestException;
import uz.pdp.appticket.mapper.CustomMapper;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.RoleAttachDto;
import uz.pdp.appticket.payload.RoleReqDto;
import uz.pdp.appticket.payload.RoleResDto;
import uz.pdp.appticket.repository.RoleRepository;
import uz.pdp.appticket.repository.UserRepository;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    public RoleServiceImpl(RoleRepository roleRepository, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @Override
    public ApiResult<RoleResDto> add(RoleReqDto roleReqDto) {
        if (roleRepository.existsByName(roleReqDto.getName()))
            throw new RestException(HttpStatus.CONFLICT, "Bunday role nomi takrorlangan!");

        Role role = new Role(
                roleReqDto.getName(),
                roleReqDto.getDescription(),
                roleReqDto.getPermissions()
        );
        roleRepository.save(role);
        return ApiResult.successResponse(CustomMapper.roleToDto(role));
    }

    @Override
    public ApiResult<?> attachRole(RoleAttachDto roleAttachDto) {
        Role role = roleRepository.findById(roleAttachDto.getRoleId()).orElseThrow(
                () -> new RestException(HttpStatus.NOT_FOUND, "Role topilmadi!"));
        List<User> userList = userRepository.findAllById(roleAttachDto.getUserIds());
        if (userList.size() != roleAttachDto.getUserIds().size())
            throw new RestException(HttpStatus.CONFLICT, "Siz xato qilyapsiz!");

        for (User user : userList) {
            user.setRole(role);
        }
        userRepository.saveAll(userList);
        return ApiResult.successResponse("Hammasi bajarildi,hurmatiz borakan");
    }

    @Override
    public ApiResult<?> getAll() {
        return ApiResult.successResponse(roleRepository.findAll().stream().map(CustomMapper::roleToDto).collect(Collectors.toList()));
    }

    @Override
    public ApiResult<RoleResDto> getPermissionsByRoleId(UUID id) {
        Role role = roleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role topilmadi!"));
        return ApiResult.successResponse(CustomMapper.roleToDto(role));
    }

    @Override
    public ApiResult<Set<PermissionEnum>> getPermissions() {
        PermissionEnum[] values = PermissionEnum.values();
        return ApiResult.successResponse(CustomMapper.permissionToSet(values));
    }


}
