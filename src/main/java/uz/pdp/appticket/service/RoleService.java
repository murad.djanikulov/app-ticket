package uz.pdp.appticket.service;

import uz.pdp.appticket.enums.PermissionEnum;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.RoleAttachDto;
import uz.pdp.appticket.payload.RoleReqDto;
import uz.pdp.appticket.payload.RoleResDto;

import java.util.Set;
import java.util.UUID;

public interface RoleService {
    ApiResult<RoleResDto> add(RoleReqDto roleReqDto);

    ApiResult<?> attachRole(RoleAttachDto roleAttachDto);

    ApiResult<?> getAll();

    ApiResult<RoleResDto> getPermissionsByRoleId(UUID id);

    ApiResult<Set<PermissionEnum>> getPermissions();

}
