package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.EventSessionReqDto;
import uz.pdp.appticket.payload.EventSessionResDto;

import java.util.List;
import java.util.UUID;

public interface EventSessionService {

    ApiResult<CustomPage<EventSessionResDto>> getAll(int page, int size);

    ApiResult<EventSessionResDto> getOne(UUID id);

    ApiResult<List<EventSessionResDto>> getByEventId(UUID eventId);

    ApiResult<EventSessionResDto> add(EventSessionReqDto eventSessionReqDto);

    ApiResult<EventSessionResDto> edit(EventSessionReqDto eventSessionReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
