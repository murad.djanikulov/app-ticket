package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.PayTypeResDto;

import java.util.List;
import java.util.UUID;

public interface PayTypeService {
    ApiResult<List<PayTypeResDto>> getAll();

    ApiResult<PayTypeResDto> get(UUID id);

    ApiResult<PayTypeResDto> create(PayTypeResDto payTypeResDto);

    ApiResult<PayTypeResDto> edit(PayTypeResDto payTypeResDto, UUID id);

    ApiResult<?> delete(UUID id);
}
