package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.PayBackTariffDto;
import uz.pdp.appticket.payload.PayBackTariffReqDto;

import java.util.UUID;

public interface PayBackTariffServise {

    ApiResult<CustomPage<PayBackTariffDto>> getAll(int page, int size);

    ApiResult<PayBackTariffDto> getById(UUID payBackTariffId);

    ApiResult<PayBackTariffDto> save(PayBackTariffReqDto payBackTariffReqDto);

    ApiResult<PayBackTariffDto> edit(PayBackTariffReqDto payBackTariffReqDto, UUID payBackTariffId);

    ApiResult<?> delete(UUID payBackTariffId);
}
