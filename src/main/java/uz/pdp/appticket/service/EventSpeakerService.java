package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.EventSpeakerReqDto;
import uz.pdp.appticket.payload.EventSpeakerResDto;

import java.util.List;
import java.util.UUID;

public interface EventSpeakerService {
    ApiResult<List<EventSpeakerResDto>> getAll();

    ApiResult<EventSpeakerResDto> getOne(UUID id);

    ApiResult<EventSpeakerResDto> add(EventSpeakerReqDto eventSpeakerReqDto);

    ApiResult<EventSpeakerResDto> edit(EventSpeakerReqDto eventSpeakerReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
