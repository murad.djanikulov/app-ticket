package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.AddressReqDto;
import uz.pdp.appticket.payload.AddressResDto;
import uz.pdp.appticket.payload.ApiResult;

import java.util.List;
import java.util.UUID;

public interface AddressService {
    ApiResult<List<AddressResDto>> getAll();

    ApiResult<AddressResDto> getAddress(UUID id);

    ApiResult<AddressResDto> add(AddressReqDto addressReqDto);

    ApiResult<AddressResDto> edit(AddressReqDto addressReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
