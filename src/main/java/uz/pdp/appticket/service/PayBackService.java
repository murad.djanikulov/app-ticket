package uz.pdp.appticket.service;

import uz.pdp.appticket.entity.User;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.RefundReqDto;
import uz.pdp.appticket.payload.RefundResDto;

public interface PayBackService {
    ApiResult<RefundResDto> refund(RefundReqDto refundReqDto, User user);

}
