package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.EventReqDto;
import uz.pdp.appticket.payload.EventResDTO;

import java.util.UUID;

public interface EventService {
    ApiResult<CustomPage<EventResDTO>> getAll(int page, int size);

    ApiResult<EventResDTO> getOne(UUID id);

    ApiResult<EventResDTO> add(EventReqDto eventReqDto);

    ApiResult<EventResDTO> edit(EventReqDto eventReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
