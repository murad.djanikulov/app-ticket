package uz.pdp.appticket.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.appticket.entity.Speaker;
import uz.pdp.appticket.exception.RestException;
import uz.pdp.appticket.mapper.CustomMapper;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SpeakerReqDto;
import uz.pdp.appticket.payload.SpeakerResDto;
import uz.pdp.appticket.repository.AttachmentRepository;
import uz.pdp.appticket.repository.SpeakerRepository;
import uz.pdp.appticket.repository.SpecializationRepository;

import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpeakerServiceImpl implements SpeakerService {

    private final SpeakerRepository speakerRepository;
    //    private final EntityMapper entityMapper;
    private final SpecializationRepository specializationRepository;
    private final AttachmentRepository attachmentRepository;

    @Override
    public ApiResult<CustomPage<SpeakerResDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "fullName", "created_at"));
        Page<Speaker> speakers = speakerRepository.findAll(pageable);
        CustomPage<SpeakerResDto> speakerDtoCustomPage = speakerToCustomPage(speakers);
        return ApiResult.successResponse(speakerDtoCustomPage);
    }

    @Override
    public ApiResult<SpeakerResDto> getOne(UUID id) {
        Speaker speaker = speakerRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Speaker topilmadi"));
        return ApiResult.successResponse(CustomMapper.speakerToDto(speaker));
    }

    @Override
    public ApiResult<SpeakerResDto> add(SpeakerReqDto speakerDto) {
        Speaker speaker = new Speaker();
        speaker.setFullName(speakerDto.getFullName());
        speaker.setDescription(speakerDto.getDescription());
        speaker.setSpecializations(new HashSet<>(specializationRepository.findAllById(speakerDto.getSpecializationsId())));
        speaker.setPhoto(attachmentRepository.findById(speakerDto.getPhotoId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Photo topilmadi")));
        speakerRepository.save(speaker);
        return ApiResult.successResponse(CustomMapper.speakerToDto(speaker));
    }

    @Override
    public ApiResult<SpeakerResDto> edit(SpeakerReqDto speakerDto, UUID id) {
        Speaker speaker = speakerRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Speaker topilmadi"));
        speaker.setFullName(speakerDto.getFullName());
        speaker.setDescription(speakerDto.getDescription());
        speaker.setSpecializations(new HashSet<>(specializationRepository.findAllById(speakerDto.getSpecializationsId())));
        speaker.setPhoto(attachmentRepository.findById(speakerDto.getPhotoId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Photo topilmadi")));
        speakerRepository.save(speaker);
        return ApiResult.successResponse(CustomMapper.speakerToDto(speaker));
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            speakerRepository.deleteById(id);
            return ApiResult.successResponse("O'chirildi");
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, "Topilmadi");
        }
    }

    public CustomPage<SpeakerResDto> speakerToCustomPage(Page<Speaker> speakerPage) {
        return new CustomPage<SpeakerResDto>(
                speakerPage.getContent().stream().map(CustomMapper::speakerToDto).collect(Collectors.toList()),
                speakerPage.getNumberOfElements(),
                speakerPage.getNumber(),
                speakerPage.getTotalElements(),
                speakerPage.getTotalPages(),
                speakerPage.getSize()
        );
    }
}
