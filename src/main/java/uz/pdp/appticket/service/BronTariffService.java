package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.BronTariffReqDto;
import uz.pdp.appticket.payload.BronTariffResDto;

import java.util.UUID;

public interface BronTariffService {

    ApiResult<BronTariffResDto> add(BronTariffReqDto bronTariffReqDto);

    ApiResult<BronTariffResDto> getOne(UUID id);

    ApiResult<?> delete(UUID id);

}
