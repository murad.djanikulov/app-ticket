package uz.pdp.appticket.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.appticket.entity.Specialization;
import uz.pdp.appticket.exception.RestException;
import uz.pdp.appticket.mapper.CustomMapper;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SpecializationDto;
import uz.pdp.appticket.payload.SpecializationReqDto;
import uz.pdp.appticket.repository.SpecializationRepository;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpecializationServiceImpl implements SpecializationService {

    private final SpecializationRepository specializationRepository;

    @Override
    public ApiResult<CustomPage<SpecializationDto>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "name"));
        Page<Specialization> specializationPage = specializationRepository.findAll(pageable);
        CustomPage<SpecializationDto> specializationDtoCustomPage = specializationDtoCustomPage(specializationPage);
        return ApiResult.successResponse(specializationDtoCustomPage);
    }

    @Override
    public ApiResult<SpecializationDto> getOne(UUID id) {
        Specialization specialization = specializationRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Mutaxassis topilmadi"));
        return ApiResult.successResponse(CustomMapper.specializationToDto(specialization));
    }

    @Override
    public ApiResult<SpecializationDto> add(SpecializationReqDto specializationReqDto) {
        Specialization specialization = new Specialization(
                specializationReqDto.getName()
        );
        Specialization specialization1 = specializationRepository.save(specialization);
        return ApiResult.successResponse(CustomMapper.specializationToDto(specialization1));
    }

    @Override
    public ApiResult<SpecializationDto> edit(SpecializationReqDto specializationReqDto, UUID uuid) {
        Specialization specialization = specializationRepository.findById(uuid).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Mutaxassis topilmadi"));
        specialization.setName(specializationReqDto.getName());
        Specialization specialization1 = specializationRepository.save(specialization);
        return ApiResult.successResponse(CustomMapper.specializationToDto(specialization1));
    }

    @Override
    public ApiResult<?> delete(UUID uuid) {
        try {
            specializationRepository.deleteById(uuid);
            return ApiResult.successResponse("Mutaxassis o'chirildi");
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, "Mutaxassis topilmadi");
        }
    }

    public CustomPage<SpecializationDto> specializationDtoCustomPage(Page<Specialization> specializationPage) {
        return new CustomPage<>(
                specializationPage.getContent().stream().map(CustomMapper::specializationToDto).collect(Collectors.toList()),
                specializationPage.getTotalPages(),
                specializationPage.getNumber(),
                specializationPage.getTotalElements(),
                specializationPage.getSize(),
                specializationPage.getNumberOfElements()
        );
    }
}
