package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SpecializationDto;
import uz.pdp.appticket.payload.SpecializationReqDto;

import java.util.UUID;

public interface SpecializationService {
    ApiResult<CustomPage<SpecializationDto>> getAll(int page, int size);

    ApiResult<SpecializationDto> getOne(UUID id);

    ApiResult<SpecializationDto> add(SpecializationReqDto specializationDto);

    ApiResult<SpecializationDto> edit(SpecializationReqDto specializationReqDto, UUID uuid);

    ApiResult<?> delete(UUID uuid);

}
