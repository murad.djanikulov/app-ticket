package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.SeatTemplateChairResDto;
import uz.pdp.appticket.payload.seatTemplate.*;

import java.util.UUID;

public interface SeatTemplateChairService {
    ApiResult<SeatTemplateDto> getAllChairsBySeatTemplate(UUID seatTemplateId);

    ApiResult<SeatTemplateChairResDto> getOneChair(UUID id);

    ApiResult<SeatTemplateDto> addSection(SectionAddDto sectionAddDto);

    ApiResult<SeatTemplateDto> addRow(RowAddDto rowAddDto);

    ApiResult<SeatTemplateDto> addChair(ChairAddDTO chairAddDTO);

    ApiResult<SeatTemplateDto> editSection(SectionEditDto sectionEditDto);

    ApiResult<SeatTemplateDto> editRow(RowEditDto rowEditDto);

    ApiResult<SeatTemplateDto> editChair(ChairEditDto chairEditDto);

    ApiResult<?> deleteSection(SectionDeleteDto sectionDeleteDto);

    ApiResult<?> deleteRow(RowDeleteDto rowDeleteDto);

    ApiResult<?> deleteChair(UUID id);
}
