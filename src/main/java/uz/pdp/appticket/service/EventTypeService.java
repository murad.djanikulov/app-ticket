package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.EventTypeReqDto;
import uz.pdp.appticket.payload.EventTypeResDto;

import java.util.List;
import java.util.UUID;

public interface EventTypeService {
    ApiResult<List<EventTypeResDto>> getAll();

    ApiResult<EventTypeResDto> get(UUID id);

    ApiResult<EventTypeResDto> add(EventTypeReqDto eventTypeReqDto);

    ApiResult<EventTypeResDto> edit(EventTypeReqDto eventTypeReqDto, UUID id);

    ApiResult<?> delete(UUID id);
}
