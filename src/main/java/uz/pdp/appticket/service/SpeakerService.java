package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SpeakerReqDto;
import uz.pdp.appticket.payload.SpeakerResDto;

import java.util.UUID;

public interface SpeakerService {

    ApiResult<CustomPage<SpeakerResDto>> getAll(int page, int size);

    ApiResult<SpeakerResDto> getOne(UUID id);

    ApiResult<SpeakerResDto> add(SpeakerReqDto speakerDto);

    ApiResult<SpeakerResDto> edit(SpeakerReqDto speakerDto, UUID id);

    ApiResult<?> delete(UUID id);
}
