package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.TicketResDto;
import uz.pdp.appticket.payload.ticket.TicketEditReqDto;
import uz.pdp.appticket.payload.ticket.TicketReqDto;

import java.util.UUID;


public interface TicketService {

    ApiResult<TicketResDto> add(TicketReqDto ticketReqDto);

    ApiResult<CustomPage<TicketResDto>> getAll(int page, int size);

    ApiResult<TicketResDto> getOne(UUID uuid);

    ApiResult<TicketResDto> edit(UUID id, TicketEditReqDto ticketEditReqDto);

    ApiResult<?> delete(UUID uuid);
}
