package uz.pdp.appticket.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.pdp.appticket.entity.Address;
import uz.pdp.appticket.exception.RestException;
import uz.pdp.appticket.mapper.CustomMapper;
import uz.pdp.appticket.payload.AddressReqDto;
import uz.pdp.appticket.payload.AddressResDto;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.repository.AddressRepository;
import uz.pdp.appticket.repository.AttachmentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AttachmentRepository attachmentRepository;

    @Override
    public ApiResult<List<AddressResDto>> getAll() {
        List<AddressResDto> addressResDtoList = new ArrayList<>();
        for (Address address : addressRepository.findAll()) {
            AddressResDto addressResDto = CustomMapper.addressToDto(address);
            addressResDtoList.add(addressResDto);
        }
        return ApiResult.successResponse(addressResDtoList);
    }

    @Override
    public ApiResult<AddressResDto> getAddress(UUID id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Bunday Address topilmadi"));
        return ApiResult.successResponse(CustomMapper.addressToDto(address));
    }

    @Override
    public ApiResult<AddressResDto> add(AddressReqDto addressDto) {
        Address address = new Address(
                addressDto.getLat(),
                addressDto.getLon(),
                addressDto.getName(),
                addressDto.getTarget(),
                attachmentRepository.findById(addressDto.getPhotoId())
                        .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "photo topilmadi"))
        );
        addressRepository.save(address);
        return ApiResult.successResponse(CustomMapper.addressToDto(address));
    }

    @Override
    public ApiResult<AddressResDto> edit(AddressReqDto addressDto, UUID id) {
        Address address = addressRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Address topilmadi"));
        address.setLat(addressDto.getLat());
        address.setLon(addressDto.getLon());
        address.setName(addressDto.getName());
        address.setTarget(addressDto.getTarget());

        address.setPhoto(attachmentRepository.findById(addressDto.getPhotoId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "photo topilmadi")));
        addressRepository.save(address);
        return ApiResult.successResponse(CustomMapper.addressToDto(address));
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            addressRepository.deleteById(id);
            return ApiResult.successResponse("O'chirildi");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Topilmadi");
        }
    }
}
