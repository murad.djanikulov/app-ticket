package uz.pdp.appticket.service;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.appticket.entity.Attachment;
import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public interface AttachmentService {
    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    ApiResult<CustomPage<Attachment>> getAll(int page, int size);

    ApiResult<Attachment> getById(UUID id);

    ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException;
}
