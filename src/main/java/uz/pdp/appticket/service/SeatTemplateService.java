package uz.pdp.appticket.service;

import uz.pdp.appticket.payload.ApiResult;
import uz.pdp.appticket.payload.CustomPage;
import uz.pdp.appticket.payload.SeatTemplateReqDto;
import uz.pdp.appticket.payload.SeatTemplateResDto;

import java.util.UUID;

public interface SeatTemplateService {

    ApiResult<CustomPage<SeatTemplateResDto>> getSeatTemplateList(int page, int size);

    ApiResult<SeatTemplateResDto> getSeatTemplateById(UUID uuid);

    ApiResult<SeatTemplateResDto> addSeatTemplate(SeatTemplateReqDto seatTemplateReqDto);

    ApiResult<SeatTemplateResDto> editSeatTemplate(SeatTemplateReqDto seatTemplateReqDto, UUID id);

    ApiResult<?> deleteSeatTemplateById(UUID id);
}
