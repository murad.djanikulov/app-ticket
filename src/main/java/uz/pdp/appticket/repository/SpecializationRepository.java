package uz.pdp.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appticket.entity.Specialization;

import java.util.UUID;

/**
 * @author Muhammad Mo'minov
 * 07.10.2021
 */
public interface SpecializationRepository extends JpaRepository<Specialization, UUID> {
}
