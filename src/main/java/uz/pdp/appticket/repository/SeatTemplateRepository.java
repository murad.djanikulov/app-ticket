package uz.pdp.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appticket.entity.SeatTemplate;

import java.util.UUID;

public interface SeatTemplateRepository extends JpaRepository<SeatTemplate, UUID> {
}
