package uz.pdp.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appticket.entity.EventType;

import java.util.UUID;

public interface EventTypeRepository extends JpaRepository<EventType, UUID> {
}
