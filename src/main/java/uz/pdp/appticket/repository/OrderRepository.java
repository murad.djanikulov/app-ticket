package uz.pdp.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import uz.pdp.appticket.entity.Order;
import uz.pdp.appticket.enums.OrderTypeEnum;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {

    List<Order> findAllByCreatedAtBeforeAndFinishedFalseAndTypeNot(Timestamp createdAt, OrderTypeEnum type);

    @Transactional
    @Modifying
    void deleteAllByCreatedAtBeforeAndFinishedFalseAndType(Timestamp pastTime, OrderTypeEnum type);
}
