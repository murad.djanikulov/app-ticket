package uz.pdp.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appticket.entity.PayBack;

import java.util.UUID;

public interface PaybackRepository extends JpaRepository<PayBack, UUID> {
}
