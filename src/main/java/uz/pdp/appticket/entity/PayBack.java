package uz.pdp.appticket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.appticket.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update pay_back set deleted=true where id=?")
@Where(clause = "deleted=false")
public class PayBack extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private PayBackTariff payBackTariff;

    private Double amount;

    private String cardNumber;

}
