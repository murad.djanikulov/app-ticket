package uz.pdp.appticket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.appticket.entity.template.AbsEntity;
import uz.pdp.appticket.enums.SeatStatusEnum;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update ticket_history set deleted=true where id=?")
@Where(clause = "deleted=false")
public class TicketHistory extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Ticket ticket;

    @Enumerated(EnumType.STRING)
    private SeatStatusEnum status;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
}
