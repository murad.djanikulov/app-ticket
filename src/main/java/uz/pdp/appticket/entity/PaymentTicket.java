package uz.pdp.appticket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import uz.pdp.appticket.entity.template.AbsEntity;
import uz.pdp.appticket.enums.PaymentTicketStatusEnum;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update payment_ticket set deleted=true where id=?")
@Where(clause = "deleted=false")
public class PaymentTicket extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private Payment payment;

    @ManyToOne(fetch = FetchType.LAZY)
    private Ticket ticket;

    @Enumerated(EnumType.STRING)
    private PaymentTicketStatusEnum paymentTicketStatus;

    private Double price;
}
