package uz.pdp.appticket.payload.seatTemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.payload.SeatTemplateChairResDto;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RowDto {
    private String name;

    private Set<SeatTemplateChairResDto> chairs;
}
