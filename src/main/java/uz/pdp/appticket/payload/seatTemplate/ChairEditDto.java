package uz.pdp.appticket.payload.seatTemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.enums.SeatStatusEnum;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChairEditDto {

    @NotNull(message = "Id bo'sh bo'lmasligi kerak")
    private UUID id;

    private Double price;

    private SeatStatusEnum seatStatusEnum;
}
