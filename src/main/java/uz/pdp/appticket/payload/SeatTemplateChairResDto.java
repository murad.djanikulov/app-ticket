package uz.pdp.appticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.enums.SeatStatusEnum;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeatTemplateChairResDto {

    private UUID id;

    private Double price;

    private SeatStatusEnum status;

    private String section;

    private String row;

    private String name;
}
