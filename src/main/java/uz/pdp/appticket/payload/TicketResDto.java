package uz.pdp.appticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.enums.SeatStatusEnum;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TicketResDto {
    private UUID id;
    private UUID userId;
    private UUID eventSessionId;
    private String section;
    private String row;
    private String name;
    private SeatStatusEnum status;
    private Double price;

}
