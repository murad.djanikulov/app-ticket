package uz.pdp.appticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.enums.OrderTypeEnum;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderResDto {

    private UUID id;

    private Double orderPrice;

    private UUID userId;

    private String[] tickets;

    private boolean finished = false;

    private OrderTypeEnum type;
}
