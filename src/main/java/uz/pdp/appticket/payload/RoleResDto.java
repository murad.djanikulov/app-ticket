package uz.pdp.appticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.enums.PermissionEnum;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResDto {
    private UUID id;

    private String name;

    private String description;

    private Set<PermissionEnum> permissions;
}
