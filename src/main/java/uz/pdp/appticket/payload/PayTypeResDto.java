package uz.pdp.appticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appticket.enums.PayTypeEnum;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PayTypeResDto {

    private UUID id;

    private String name;

    private PayTypeEnum payTypeEnum;
}
