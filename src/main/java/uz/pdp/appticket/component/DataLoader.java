package uz.pdp.appticket.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.appticket.entity.PayType;
import uz.pdp.appticket.entity.Role;
import uz.pdp.appticket.entity.User;
import uz.pdp.appticket.enums.PayTypeEnum;
import uz.pdp.appticket.enums.PermissionEnum;
import uz.pdp.appticket.repository.PayTypeRepository;
import uz.pdp.appticket.repository.RoleRepository;
import uz.pdp.appticket.repository.UserRepository;
import uz.pdp.appticket.utils.AppConstant;

import java.util.Arrays;
import java.util.HashSet;

@Component
public class DataLoader implements CommandLineRunner {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final PayTypeRepository payTypeRepository;

    public DataLoader(RoleRepository roleRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, PayTypeRepository payTypeRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.payTypeRepository = payTypeRepository;
    }

    @Value("${dataLoaderMode}")
    private String dataLoaderMode;


    @Override
    public void run(String... args) throws Exception {

        if (dataLoaderMode.equals("always")) {
            Role admin = roleRepository.save(new Role(
                    AppConstant.ADMIN,
                    "Admin",
                    new HashSet<>(Arrays.asList(PermissionEnum.values()))));


            userRepository.save(new User(
                    "admin",
                    "admin",
                    "+998971234567",
                    passwordEncoder.encode("admin123"),
                    admin,
                    null,
                    true));

            roleRepository.save(new Role(
                    AppConstant.USER,
                    "Client",
                    new HashSet<>()));

            payTypeRepository.saveAll(Arrays.asList(
                    new PayType("", PayTypeEnum.CARD)
                    , new PayType(" ", PayTypeEnum.TRANSFER))
            );

        }
    }
}
