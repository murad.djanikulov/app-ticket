package uz.pdp.appticket.enums;

public enum PayTypeEnum {
    TRANSFER("Pul utkazish"),
    CARD("Karta orqali");

    private final String nameUz;

    PayTypeEnum(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUz() {
        return nameUz;
    }
}
