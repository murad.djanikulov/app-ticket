package uz.pdp.appticket.enums;

public enum BronStatusEnum {
    ACTIVE,
    COMPLETE,
    CANCEL
}
